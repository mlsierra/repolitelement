import { LitElement, html, css } from 'lit-element';
import '../persona-ficha-listado/persona.ficha-listado.js';
import '../persona-form/persona-form.js';


class PersonaMain extends LitElement {
    
    //static get styles(){
    //    return css`
    //       :host {
    //          all: initial;
    //        }
    //       }
    //    `;
    //    }

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };

    }

    constructor(){
        super();
        this.people=[
            {
                fname: "Brat Pitt",
                yearsInCompany: 12,
                photo: {            
                src:"./image/persona.jpg",
                alt:  "Brat Pitt"
                },
                profile: "Lorem ipsum dolor sat amet."
            },{
                fname: "Bruce Willis",
                yearsInCompany: 8,
                photo: {            
                    src:"./image/persona.jpg",
                    alt: "Bruce Willis"
                    },
                profile: "Lorem ipsum dolor set emet Lorem ipsum dolor set emet Lorem ipsum dolor set emet."
            },{
                fname: "Tom Cruise",
                yearsInCompany: 3,
                photo: {            
                    src:"./image/persona.jpg",
                    alt: "Tom Cruise"
                    },
                profile: "Lorem ipsum dolor sit imet."
            },{
                fname: "Harry Potter",
                yearsInCompany: 6,
                photo: {            
                    src:"./image/persona.jpg",
                    alt: "Harry Potter"
                    },
                profile: "Lorem ipsum dolor sot omet."
            },{
                fname: "Di Caprio",
                yearsInCompany: 5,
                photo: {            
                    src:"./image/persona.jpg",
                    alt: "Di Caprio"
                    },
                profile: "Lorem ipsum dolor sut umet Lorem ipsum dolor sut umet."
            }

        ];
        this.showPersonForm=false;

    }

    updated(changedProperties) {          
        console.log("updated");          
        if (changedProperties.has("showPersonForm")) {             
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");             
            if (this.showPersonForm === true) {                 
                this.showPersonFormData();             
            } else {                 
                this.showPersonList();             
            }         
        }  
        
        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad  people en persona-main");
            this.dispatchEvent(
                new CustomEvent("update-people", {
                    detail:{
                        people : this.people
                    } 
                })
            );
        }
    }

    showPersonFormData() {         
        console.log("showPersonFormData");         
        console.log("Mostrando formulario de persona");         
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");            
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");      
    } 

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }
    render(){
        return html`
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
          <h2 class="text-center">Personas </h2>

          <div class="row" id="peopleList"> 
               <div class="row row-cols-1 row-cols-sm-5">
                 ${this.people.map(
                   person => html`<persona-ficha-listado fname="${person.fname}" 
                                                      yearsInCompany="${person.yearsInCompany}"
                                                      profile="${person.profile}"
                                                      .photo="${person.photo}"
                                                      @delete-person="${this.deletePerson}"
                                                      @info-person="${this.infoPerson}"
                                >
                               </persona-ficha-listado>`
                  )}
                 
               </div>
               
          </div>
          <div class="row">
            <persona-form id="personForm" class="d-none border rounded border-primary"                               
              @persona-form-close="${this.personFormClose}"                               
              @persona-form-store="${this.personFormStore}">                 
            </persona-form>
              
          </div>
       
        `;
          
    }
    deletePerson(e) { 
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.fname);
        this.people = this.people.filter(
            person => person.fname != e.detail.fname
        );

    }

   personFormClose(e) {  
       this.showPersonForm=false;   
       console.log("personFormClose");         
       console.log("Se ha cerrado el formulario de la persona");         
    }     
    personFormStore(e) {         
        console.log("personFormStore");         
        console.log("Se va a almacenar una persona");  
        if (e.detail.editingPerson){
            
            console.log("Persona modificada:  " + e.detail.person.fname);
            /* let indexOFPerson = 
                this.people.findIndex(
                    person => person.fname === e.detail.person.fname
                );
            
            console.log("El indice a actualizar es: " + indexOFPerson);                
            if (indexOFPerson >= 0){
                console.log("Persona actualizada");
                this.people[indexOFPerson] = e.detail.person;
            }
            */
            
            this.people= this.people.map(
                person => person.fname === e.detail.person.fname 
                ?  person = e.detail.person : person 
            );
            
        } else {
            //this.people.push(e.detail.person);
            this.people  = [...this.people, e.detail.person];
            console.log("Persona almacenada " + e.detail.person.fname);
        }                                   
        console.log("Proceso terminado ");               
                  
        this.showPersonForm = false;     
    }

    infoPerson(e){
        console.log("inforperson en persona.main");
        console.log("Se ha pedido información de la persona:" +e.detail.fname)

        let choosenPerson = this.people.filter(
            person => person.fname === e.detail.fname

        )
        //console.log(choosenPerson);
        this.shadowRoot.getElementById("personForm").person= choosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson= true;
        this.showPersonForm=true;
    }

}

customElements.define('persona-main', PersonaMain)