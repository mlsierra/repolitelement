import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

    static get properties(){
        return {
            people: {type: Array}

        };

    }

    constructor(){
        super();
        this.people= [];

    }
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
            <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
            <persona-main @update-people="${this.updatePeople}" class="col-10"></persona-main>
        </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"> </persona-stats>

          `;
    }

    peopleStatsUpdated(e){
        console.log("peopleStatsUpdated in pesona-app");

    }


    peopleStatsUpdated(e){
       console.log("peopleStatsUpdated");
       this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;

    }
    updatePeople(e){
        console.log("updatePeople");
        this.people = e.detail.people; 
    }

    updated(changedProperties){
        console.log("updated in pesona-app");
        if (changedProperties.has("people")) {
            console.log("ha cambiado people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    newPerson(e) {         
        console.log("newPerson en PersonaApp");          
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;     
    }   
}



customElements.define('persona-app', PersonaApp)