import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {
    static get properties(){
        return {
            movies: {type: Array}
        };
    }

    constructor(){
        super();
        this.movies=[];
        this.getMovieData();
    }

    render(){
        return html`
          ${this.movies.map(
              movies => 
                 html` <div> La película  ${movies.title}, fue dirigida por ${movies.director}.</div>`
          )}
          
          `;
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de la pelicula");

        let xhr = new XMLHttpRequest();
        xhr.open("GET","https://swapi.dev/api/films");
        xhr.send();
        console.log("Información enviada");

        
        xhr.onload=()=>{
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                this.movies = APIResponse.results;
            }

        }
    }
}

customElements.define('test-api', TestApi)